package br.com.maino.instasales;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by IRM on 21/08/13.
 */
public class TagAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    JSONArray tags;
    Context context;

    class ViewHolder{
        TextView name;
        TextView taggings;
    }

    public TagAdapter(Context context, JSONArray tags){
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.tags = tags;
    }

    @Override
    public int getCount() {
        return tags.length();
    }

    @Override
    public Object getItem(int i) {

        try {
            return tags.get(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public long getItemId(int i) {
        JSONObject json = null;
        try {
            json = (JSONObject) tags.get(i);
            return json.getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder holder;
        JSONObject tag = null;

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.tag_layout, null);

            // Set up the ViewHolder.
            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.taggings = (TextView) view.findViewById(R.id.taggings);


            // Store the holder with the view.
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        try {

            //Get JSON Object
            tag = (JSONObject) tags.get(i);

            // Assign values
            if (tag != null){
                holder.name.setText(tag.getString("name"));
                holder.taggings.setText(tag.getInt("taggings") + " products");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            view.setOnClickListener(new OnClickListenerTagAdapter(context, tag.getString("name")));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    class OnClickListenerTagAdapter implements View.OnClickListener {

        Context context = null;
        String searchString = null;

        public OnClickListenerTagAdapter(Context context, String searchString){
            this.context = context;
            this.searchString = searchString;
        }

        @Override
        public void onClick(View view) {
            ProductsSearchFragment fragment = ProductsSearchFragment.newInstance(context, searchString);
            DrawerActivity.getInstance().selectFragment(fragment, searchString, 2);
        }
    }
}
