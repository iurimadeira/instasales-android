package br.com.maino.instasales.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

/**
 * Created by IRM on 21/08/13.
 */
public class ImageEncodingService {

    public static String fileToString(Uri uri){
        Bitmap bitmap = BitmapFactory.decodeFile(uri.toString());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] byteArrayImage = baos.toByteArray();

        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
    }

    public static String bitmapToString(Bitmap bitmap){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] byteArrayImage = baos.toByteArray();

        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
    }

    public static Bitmap stringToBitmap(String encodedString){
        byte[] imageByte = Base64.decode(encodedString, Base64.DEFAULT);

        return BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
    }
}
