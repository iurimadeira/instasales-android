package br.com.maino.instasales;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.maino.instasales.util.ConnectionService;

/**
 * Created by IRM on 21/08/13.
 */
public class UserAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    JSONArray users;
    SearchFragment fragment;
    Context context;

    class ViewHolder{
        TextView email;
        Button button_follow;
    }


    public UserAdapter(Context context, SearchFragment fragment, JSONArray users){
        this.context = context;
        this.fragment = fragment;
        this.inflater = LayoutInflater.from(context);
        this.users = users;
    }

    @Override
    public int getCount() {
        return users.length();
    }

    @Override
    public Object getItem(int i) {

        try {
            return users.get(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public long getItemId(int i) {
        JSONObject json = null;
        try {
            json = (JSONObject) users.get(i);
            return json.getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder holder;
        JSONObject user = null;

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.user_layout, null);

            // Set up the ViewHolder.
            holder = new ViewHolder();
            holder.email = (TextView) view.findViewById(R.id.email);
            holder.button_follow = (Button) view.findViewById(R.id.button_follow);

            // Store the holder with the view.
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        try {

            //Get JSON Object
            user = (JSONObject) users.get(i);

            // Assign values
            if (user != null){

                holder.email.setText(user.getString("email"));
                Log.d("User", "following: " + user.getBoolean("following"));
                if (user.getBoolean("following")){
                    holder.button_follow.setText(R.string.button_unfollow);
                    holder.button_follow.setOnClickListener(new OnClickListenerFollow(user.getInt("id"), false, this, holder));
                } else {
                    holder.button_follow.setText(R.string.button_follow);
                    holder.button_follow.setOnClickListener(new OnClickListenerFollow(user.getInt("id"), true, this, holder));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    private void follow(Integer id, boolean follow, ViewHolder holder){

        JSONObject params = new JSONObject();

        try {
            params.put("auth_token", fragment.getArguments().getString("auth_token"));
            params.put("followed_id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (follow){
            FollowTask followTask = new FollowTask(holder);
            followTask.execute(params);
        } else {
            UnfollowTask unfollowTask = new UnfollowTask(holder);
            unfollowTask.execute(params);
        }

    }

    class OnClickListenerFollow implements View.OnClickListener {

        int id;
        boolean follow;
        UserAdapter userAdapter;
        ViewHolder holder;

        public OnClickListenerFollow(int id, boolean follow, UserAdapter userAdapter, ViewHolder holder){
            this.id = id;
            this.follow = follow;
            this.userAdapter = userAdapter;
            this.holder = holder;
        }

        @Override
        public void onClick(View view) {
            userAdapter.follow(id, follow, holder);
        }
    }

    class FollowTask extends AsyncTask<JSONObject, Void, JSONArray> {

        String root = null;
        String follow = null;

        ViewHolder holder = null;

        FollowTask(ViewHolder holder){
            this.holder = holder;
        }

        @Override
        protected void onPreExecute(){
            root = context.getResources().getString(R.string.root);
            follow = context.getResources().getString(R.string.follow);
        }

        @Override
        protected JSONArray doInBackground(JSONObject... params) {

            JSONArray followJson = ConnectionService.doRequest(root + follow, params[0], "POST");

            return followJson;
        }

        @Override
        protected void onPostExecute(final JSONArray followJson){
            fragment.holder.button_users_search.performClick();
        }
    }

    class UnfollowTask extends AsyncTask<JSONObject, Void, JSONArray> {

        String root = null;
        String unfollow = null;

        ViewHolder holder = null;

        UnfollowTask(ViewHolder holder){
            this.holder = holder;
        }

        @Override
        protected void onPreExecute(){
            root = context.getResources().getString(R.string.root);
            unfollow = context.getResources().getString(R.string.unfollow);
        }

        @Override
        protected JSONArray doInBackground(JSONObject... params) {

            JSONArray unfollowJson = ConnectionService.doRequest(root + unfollow, params[0], "DELETE");

            return unfollowJson;
        }

        @Override
        protected void onPostExecute(final JSONArray unfollowJson){
            fragment.holder.button_users_search.performClick();
        }
    }
}
