package br.com.maino.instasales;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.maino.instasales.util.ConnectionService;

public class UsersCreate extends Activity {

    ViewHolder holder;
    CreateUserTask task;

    class ViewHolder{
        TextView email;
        TextView password;
        TextView password_confirmation;
        Button sign_up_button;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_create);

        holder = new ViewHolder();
        holder.email = (TextView) findViewById(R.id.email);
        holder.password = (TextView) findViewById(R.id.password);
        holder.password_confirmation = (TextView) findViewById(R.id.password_confirmation);
        holder.sign_up_button = (Button) findViewById(R.id.sign_up_button);

        holder.sign_up_button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createUser();
            }
        });
    }

    public void createUser(){

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password.
        if (TextUtils.isEmpty(holder.password.getText().toString())){
            holder.password.setError(getString(R.string.error_field_required));
            focusView = holder.password;
            cancel = true;
        } else if (holder.password.getText().toString().length() < 4 || holder.password_confirmation.getText().toString().length() < 4) {
            holder.password.setError(getString(R.string.error_invalid_password));
            focusView = holder.password;
            cancel = true;
        }

        //Check wheter password and confirmation are equal.
        if (!holder.password.getText().toString().equals(holder.password_confirmation.getText().toString())){
            holder.password.setError(getString(R.string.error_password_not_confirmed));
            holder.password_confirmation.setError(getString(R.string.error_password_not_confirmed));
            focusView = holder.password;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(holder.email.getText().toString())) {
            holder.email.setError(getString(R.string.error_field_required));
            focusView = holder.email;
            cancel = true;
        } else if (!holder.email.getText().toString().contains("@")) {
            holder.email.setError(getString(R.string.error_invalid_email));
            focusView = holder.email;
            cancel = true;
        }

        JSONObject json = new JSONObject();
        JSONObject user = new JSONObject();

        if (cancel){
            focusView.requestFocus();
        } else {

            try {
                user.put("email", holder.email.getText());
                user.put("password", holder.password.getText());
                json.put("user", user);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            task = new CreateUserTask();
            task.execute(json);

        }
    }

    public class CreateUserTask extends AsyncTask<JSONObject, Void, String> {

        String sign_up = null;
        String root = null;

        @Override
        protected void onPreExecute(){
            root = getResources().getString(R.string.root);
            sign_up = getResources().getString(R.string.sign_up);
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            try {
                JSONArray userArray = ConnectionService.doRequest(root + sign_up, params[0], "POST");
                JSONObject userObject = (JSONObject) userArray.get(0);
                return userObject.getString("auth_token");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(final String token) {

            if (token != null) {
                Intent intent = new Intent(UsersCreate.this, LoginActivity.class);
                Log.d("UsersCreate -> LoginActivity", "User created!");
                startActivity(intent);
            }

        }

        @Override
        protected void onCancelled() {
            task = null;
        }
    }
    
}
