package br.com.maino.instasales;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import br.com.maino.instasales.util.ImageEncodingService;
import br.com.maino.instasales.util.TimeAgo;

/**
 * Created by IRM on 21/08/13.
 */
public class ProductAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    JSONArray products;
    Context context;

    class ViewHolder{
        TextView email;
        ImageView image;
        TextView description;
        TextView created_at;
        TextView price;
    }

    public ProductAdapter(Context context, JSONArray products){
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.products = products;
    }

    @Override
    public int getCount() {
        return products.length();
    }

    @Override
    public Object getItem(int i) {

        try {
            return products.get(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public long getItemId(int i) {
        JSONObject json = null;
        try {
            json = (JSONObject) products.get(i);
            return json.getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder holder;
        JSONObject product = null;

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.product_layout, null);

            // Set up the ViewHolder.
            holder = new ViewHolder();
            holder.email = (TextView) view.findViewById(R.id.email);
            holder.image = (ImageView) view.findViewById(R.id.image);
            holder.description = (TextView) view.findViewById(R.id.description);
            holder.created_at = (TextView) view.findViewById(R.id.created_at);
            holder.price = (TextView) view.findViewById(R.id.price);

            // Store the holder with the view.
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        try {

            //Get JSON Object
            product = (JSONObject) products.get(i);

            // Assign values
            if (product != null){
                holder.image.setImageBitmap(ImageEncodingService.stringToBitmap(product.getString("image")));
                holder.email.setText(product.getJSONObject("user").getString("email"));
                holder.description.setText(product.getString("description"));
                holder.price.setText("R$ " + product.getString("price"));

                try {
                    Date createdAt = (new SimpleDateFormat()).parse(product.getString("created_at"));

                    holder.created_at.setText(TimeAgo.getTimeAgo(createdAt.getTime(), this.context));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }
}
