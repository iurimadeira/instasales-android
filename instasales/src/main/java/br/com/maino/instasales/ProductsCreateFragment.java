package br.com.maino.instasales;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.maino.instasales.util.ImageEncodingService;
import br.com.maino.instasales.util.ConnectionService;

public class ProductsCreateFragment extends Fragment {

    static final int CAMERA_REQUEST = 1111;

    CreateProductTask task;

    JSONObject product = new JSONObject();
    ViewHolder holder;

    class ViewHolder{
        ImageView image_preview;
        TextView description;
        Button button_sell;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.products_create_fragment, null);

        //Passes the views to the ViewHolder
        holder = new ViewHolder();
        holder.image_preview = (ImageView) root.findViewById(R.id.image_preview);
        holder.description = (TextView) root.findViewById(R.id.description);
        holder.button_sell = (Button) root.findViewById(R.id.button_sell);

        //Sets onClickListener to button_sell
        holder.button_sell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createProduct();
            }
        });

        return root;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Starts camera activity for result
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == CAMERA_REQUEST) {
            //Gets the result image
            Bitmap photo = (Bitmap) data.getExtras().get("data");

            //Converts image to a String and pack it to send
            try {
                product.put("image", ImageEncodingService.bitmapToString(photo));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //Shows it in image_preview
            holder.image_preview.setImageBitmap(photo);
        }
    }

    public void createProduct(){
        JSONObject json = new JSONObject();

        try {
            product.put("description", holder.description.getText());
            json.put("product", product);
            json.put("auth_token", getArguments().getString("auth_token"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        task = new CreateProductTask();
        task.execute(json);
    }

    public class CreateProductTask extends AsyncTask<JSONObject, Void, String> {

        String products = null;
        String root = null;

        @Override
        protected void onPreExecute(){
            root = getResources().getString(R.string.root);
            products = getResources().getString(R.string.products);
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            ConnectionService.doRequest(root + products, params[0], "POST");
            return getArguments().getString("auth_token");
        }

        @Override
        protected void onPostExecute(final String token) {

            if (token != null) {
                Intent intent = new Intent(getActivity(), DrawerActivity.class);
                intent.putExtra("auth_token", token);
                Log.d("ProductsCreateFragment -> ProductsIndex", "Token: " + token);
                startActivity(intent);
            }

        }

        @Override
        protected void onCancelled() {
            task = null;
        }
    }

}
