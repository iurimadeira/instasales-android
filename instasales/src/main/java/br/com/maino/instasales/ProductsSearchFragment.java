package br.com.maino.instasales;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.maino.instasales.util.ConnectionService;

public class ProductsSearchFragment extends Fragment {

    ProductsSearchTask productsSearchTask;
    ViewHolder holder;
    String searchString = null;

    class ViewHolder{
        ListView products_list;
    }

    public static ProductsSearchFragment newInstance(Context context, String searchString) {
        ProductsSearchFragment fragment = new ProductsSearchFragment();
        fragment.searchString = searchString;

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.products_index_fragment, null);

        holder = new ViewHolder();
        holder.products_list = (ListView) root.findViewById(R.id.products_list);

        return root;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JSONObject params = new JSONObject();

        try {
            params.put("auth_token", getArguments().getString("auth_token"));
            params.put("tag", searchString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        productsSearchTask = new ProductsSearchTask();
        productsSearchTask.execute(params);
    }

    public class ProductsSearchTask extends AsyncTask<JSONObject, Void, JSONArray> {

        String root = null;
        String products_path = null;
        String search = null;

        @Override
        protected void onPreExecute(){
            root = getResources().getString(R.string.root);
            products_path = getResources().getString(R.string.products);
            search = getResources().getString(R.string.search);
        }

        @Override
        protected JSONArray doInBackground(JSONObject... params) {

            JSONArray products = ConnectionService.doRequest(root + products_path + search, params[0], "GET");

            return products;
        }

        @Override
        protected void onPostExecute(final JSONArray products) {

            Context context = ProductsSearchFragment.this.getActivity();

            if (products != null && context != null) {
                ProductAdapter adapter = new ProductAdapter(context, products);
                holder.products_list.setAdapter(adapter);
            } else {
                //Throw exception;
            }
        }

        @Override
        protected void onCancelled() {
            productsSearchTask = null;
        }
    }
    
}
