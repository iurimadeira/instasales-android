package br.com.maino.instasales;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.maino.instasales.util.ConnectionService;

public class SearchFragment extends Fragment {

    ViewHolder holder;
    UsersSearchTask usersSearchTask;
    TagsSearchTask tagsSearchTask;

    class ViewHolder{
        EditText edit_search;
        ListView result_list;
        Button button_users_search, button_tags_search;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.search_fragment, null);

        holder = new ViewHolder();
        holder.edit_search = (EditText) root.findViewById(R.id.edit_search);
        holder.result_list = (ListView) root.findViewById(R.id.result_list);
        holder.button_users_search = (Button) root.findViewById(R.id.button_users_search);
        holder.button_tags_search= (Button) root.findViewById(R.id.button_tags_search);

        holder.button_users_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchUsers();
            }
        });

        holder.button_tags_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                searchTags();
            }
        });

        return root;
    }

    public void searchUsers(){
        JSONObject params = new JSONObject();

        try {
            params.put("auth_token", getArguments().getString("auth_token"));
            params.put("user", holder.edit_search.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        usersSearchTask = new UsersSearchTask();
        usersSearchTask.execute(params);
    }

    public void searchTags(){
        JSONObject params = new JSONObject();

        try {
            params.put("auth_token", getArguments().getString("auth_token"));
            params.put("name", holder.edit_search.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        tagsSearchTask = new TagsSearchTask();
        tagsSearchTask.execute(params);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public class UsersSearchTask extends AsyncTask<JSONObject, Void, JSONArray> {

        String root = null;
        String users_path = null;
        String search = null;

        @Override
        protected void onPreExecute(){
            root = getResources().getString(R.string.root);
            users_path = getResources().getString(R.string.users);
            search = getResources().getString(R.string.search);
        }

        @Override
        protected JSONArray doInBackground(JSONObject... params) {
            JSONArray users = ConnectionService.doRequest(root + users_path + search, params[0], "GET");

            return users;
        }

        @Override
        protected void onPostExecute(final JSONArray users) {

            Context context = SearchFragment.this.getActivity();

            if (users != null && context != null) {
                UserAdapter adapter = new UserAdapter(context, SearchFragment.this, users);
                holder.result_list.setAdapter(adapter);
            } else {
                //Throw exception;
            }
        }

        @Override
        protected void onCancelled() {
            usersSearchTask = null;
        }
    }

    public class TagsSearchTask extends AsyncTask<JSONObject, Void, JSONArray> {

        String root = null;
        String tags_path = null;
        String search = null;

        @Override
        protected void onPreExecute(){
            root = getResources().getString(R.string.root);
            tags_path = getResources().getString(R.string.tags);
            search = getResources().getString(R.string.search);
        }

        @Override
        protected JSONArray doInBackground(JSONObject... params) {
            JSONArray tags = ConnectionService.doRequest(root + tags_path + search, params[0], "GET");

            return tags;
        }

        @Override
        protected void onPostExecute(final JSONArray tags) {

            Context context = SearchFragment.this.getActivity();

            if (tags != null && context != null) {
                TagAdapter adapter = new TagAdapter(context , tags);
                holder.result_list.setAdapter(adapter);
            } else {
                //Throw exception;
            }
        }

        @Override
        protected void onCancelled() {
            tagsSearchTask = null;
        }
    }
    
}
