package br.com.maino.instasales;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.maino.instasales.util.ConnectionService;

public class ProductsIndexFragment extends Fragment {

    ProductsIndexTask productsIndexTask;
    ViewHolder holder;

    class ViewHolder{
        ListView products_list;
    }

    public static Fragment newInstance(Context context) {
        ProductsIndexFragment fragment = new ProductsIndexFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.products_index_fragment, null);

        holder = new ViewHolder();
        holder.products_list = (ListView) root.findViewById(R.id.products_list);

        return root;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JSONObject params = new JSONObject();

        try {
            params.put("auth_token", getArguments().getString("auth_token"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        productsIndexTask = new ProductsIndexTask();
        productsIndexTask.execute(params);
    }

    public class ProductsIndexTask extends AsyncTask<JSONObject, Void, JSONArray> {

        String root = null;

        @Override
        protected void onPreExecute(){
            root = getResources().getString(R.string.root);
        }

        @Override
        protected JSONArray doInBackground(JSONObject... params) {

            JSONArray products = ConnectionService.doRequest(root, params[0], "GET");

            return products;
        }

        @Override
        protected void onPostExecute(final JSONArray products) {

            Context context = ProductsIndexFragment.this.getActivity();

            if (products != null && context != null) {
                ProductAdapter adapter = new ProductAdapter(context , products);
                holder.products_list.setAdapter(adapter);
            } else {
                //Throw exception;
            }
        }

        @Override
        protected void onCancelled() {
            productsIndexTask = null;
        }
    }
    
}
