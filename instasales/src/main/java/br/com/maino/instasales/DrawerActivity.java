package br.com.maino.instasales;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Arrays;

public class DrawerActivity extends FragmentActivity {

    private String[] menuItems, fragments;
    private ViewHolder holder;
    private static DrawerActivity instance = null;

    //Declares the view holder for boost the performance
    class ViewHolder{
        DrawerLayout drawer_layout;
        ListView drawer_list;
    }

    static {
        instance = new DrawerActivity();
    }

    public static DrawerActivity getInstance(){
        return instance;
    }

    //Class that acts as a listener for the Drawer Menu
    class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.drawer_activity);

        //Creates an instance of the view holder and gets its layouts
        holder = new ViewHolder();
        holder.drawer_layout = (DrawerLayout) findViewById(R.id.drawer_activity);
        holder.drawer_list = (ListView) findViewById(R.id.left_drawer);

        //Gets the menu items
        menuItems = getResources().getStringArray(R.array.drawer_menu_items);
        fragments = getResources().getStringArray(R.array.drawer_menu_fragments);

        // Set the adapter for the list view
        holder.drawer_list.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_item_layout, R.id.drawer_item, menuItems));

        // Set the list's click listener
        holder.drawer_list.setOnItemClickListener(new DrawerItemClickListener());

        //Calls home fragment
        selectFragment(new ProductsIndexFragment());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    public void selectItem(int position){
        Log.d("Fragment Selected", menuItems[position] + " | " + fragments[position]);
        selectFragment(Fragment.instantiate(DrawerActivity.this, fragments[position]));
    }

    public void selectFragment(Fragment fragment, String title, int index) {

        // Create a new fragment and specify the planet to show based on position
        Bundle args = new Bundle();
        args.putString("auth_token", getIntent().getStringExtra("auth_token"));
        fragment.setArguments(args);

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();

        // Highlight the selected item, update the title, and close the drawer
        Log.d("DrawerActivity", "Fragment index: " + index);
        holder.drawer_list.setItemChecked(index, true);
        setTitle(title);
        holder.drawer_layout.closeDrawer(holder.drawer_list);
    }

    public void selectFragment(Fragment fragment){
        int indexOfFragment = Arrays.asList(fragments).indexOf(fragment.getClass().getName());
        String title = menuItems[indexOfFragment];

        selectFragment(fragment, title, indexOfFragment);
    }

}
