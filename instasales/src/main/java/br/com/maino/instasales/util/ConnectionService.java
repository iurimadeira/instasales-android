package br.com.maino.instasales.util;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Class that connects to WebService
 */
public class ConnectionService {

    public static JSONArray doRequest(String address, JSONObject json, String method){

        if (method.equals("GET")){
            return doGetDeleteRequest(address, json, "GET");
        } else if (method.equals("DELETE")){
            return doGetDeleteRequest(address, json, "DELETE");
        }

        InputStream inputStream = null;
        HttpURLConnection urlConnection = null;
        OutputStream outputStream = null;
        JSONArray jsonArray = null;

        try {
            URL url = new URL (address);
            urlConnection = (HttpURLConnection) url.openConnection();
            String message = json.toString();

            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setRequestMethod(method);
            urlConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            urlConnection.setFixedLengthStreamingMode(message.getBytes().length);
            urlConnection.connect();

            Log.d("JSON Request", json.toString() + " | to: " + address);

            outputStream = urlConnection.getOutputStream();
            outputStream.write(message.getBytes("UTF-8"));
            outputStream.flush();

            inputStream = urlConnection.getInputStream();
            jsonArray = streamToJson(inputStream);

        } catch (IOException e){
            e.printStackTrace();
        } finally {

            try {

                if (inputStream != null) inputStream.close();
                if (outputStream != null) outputStream.close();
                if (urlConnection != null) urlConnection.disconnect();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return jsonArray;

    }

    private static JSONArray doGetDeleteRequest(String address, JSONObject json, String method){

        InputStream inputStream = null;
        HttpURLConnection urlConnection = null;
        OutputStream outputStream = null;
        JSONArray jsonArray = null;

        try {
            URL url = new URL (address + jsonToGetParams(json));
            urlConnection = (HttpURLConnection) url.openConnection();
            String message = json.toString();

            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setDoOutput(false);
            urlConnection.setDoInput(true);
            urlConnection.setRequestMethod(method);
            urlConnection.connect();

            Log.d(method + " Request", url.toString());

            inputStream = urlConnection.getInputStream();
            jsonArray = streamToJson(inputStream);

        } catch (IOException e){
            e.printStackTrace();
        } finally {

            try {

                if (inputStream != null) inputStream.close();
                if (outputStream != null) outputStream.close();
                if (urlConnection != null) urlConnection.disconnect();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return jsonArray;

    }

    private static String jsonToGetParams(JSONObject json){

        StringBuffer params = new StringBuffer();
        params.append("?");

        try {
            Iterator<?> keys = json.keys();
            while( keys.hasNext() ){
                String key = (String) keys.next();

                    params.append(key + "=" + json.get(key));


                if (keys.hasNext()) params.append("&");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return params.toString();
    }

    private static String streamToString(InputStream inputStream){

        BufferedReader bufferedReader;
        StringBuffer string = new StringBuffer();
        String line;

        try {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            while ((line = bufferedReader.readLine()) != null) {
                string.append(line);
            }
        } catch (IOException e ){
            e.printStackTrace();
        }

        Log.d("JSON Response", string.toString() + " | Size: " + string.toString().getBytes().length);
        return string.toString();
    }

    private static JSONArray streamToJson(InputStream inputStream){
        String jsonString = streamToString(inputStream);

        try {
            if (jsonString.startsWith("[")){
                return new JSONArray(jsonString);
            } else {
                JSONArray array = new JSONArray();
                array.put(new JSONObject(jsonString));

                return array;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
